let counter=document.getElementById("count");
let startButton=document.getElementById('start');
let stopButton=document.getElementById('stop');
let resetButton=document.getElementById('reset');
let count=0;
let intervalId=null;
counter.textContent=count;
startButton.addEventListener("click",()=>{
    // this if block is used for, if user click multiple time then also timer will increase by one only
    if(intervalId){
        return
    }
    intervalId=setInterval(()=>{
        startTimer();
    },1000)    
});
resetButton.addEventListener('click',resetTimer);
stopButton.addEventListener('click',stopTimer);
// this function is used for start the count
function startTimer(){
    count++;
    counter.textContent=count;
}
// this function is used for reset the count
function resetTimer(){
    count=0;
    counter.textContent=count;
    clearInterval(intervalId);
    intervalId=null;
}
// this function is used for stop the count
function stopTimer(){
    clearInterval(intervalId);
    intervalId=null;
}

